import { getPeak } from './getPeak'

export const getMaxTotalSize = (orders: number[][]): number => {
  const totals = orders.map(([price, size, total]) => total)
  return getPeak.max(totals)
}

export const setTotalSize = (orders: number[][]) => {
  return orders.reduce((acc: number[][], order: number[], i: number) => {
    if (i === 0) {
      return [...acc, [...order, order[1]]]
    } else {
      return [...acc, [...order, order[1] + acc[i - 1][2]]]
    }
  }, [])
}

export const setDepth = (orders: number[][], maxTotal: number) => {
  return orders.reduce((acc: number[][], order: number[], i: number) => {
    const total: number = order[2]
    const depthPercent = (total / maxTotal) * 100
    return [...acc, [...order, depthPercent]]
  }, [])
}
