import { roundToInterval } from './roundToInterval'

const groupByUniquePrice = (
  orders: number[][],
  ordersSize?: number,
): number[][] => {
  const map = new Map()

  for (let i = 0; i < orders.length; i++) {
    if (ordersSize && map.size === ordersSize) {
      break
    } else {
      const [price, size] = orders[i]
      const sizePrice = map.get(price)
      if (sizePrice) {
        map.set(price, size + sizePrice)
      } else {
        map.set(price, size)
      }
    }
  }

  return Array.from(map)
}

export const groupByTickPrice = (
  orders: string[][],
  tick: number,
  ordersSize?: number,
): number[][] => {
  return groupByUniquePrice(
    orders.map(([price, size]) => [
      roundToInterval(Number(price), tick),
      Number(size),
    ]),
    ordersSize,
  )
}
