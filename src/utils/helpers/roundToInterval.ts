export const roundToInterval = (price: number, interval: number) =>
  Math.floor(Number(price) / interval) * interval
