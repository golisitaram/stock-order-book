export const filterZeroSizeOrders = (
  price: number,
  orders: number[][],
): number[][] => {
  return orders.filter(([oprice, osize]) => Number(oprice) !== Number(price))
}
