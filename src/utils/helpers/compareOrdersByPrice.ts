import { OrderType } from 'src/constants'

export const compareOrdersByPrice = (
  orders: number[][],
  type: string,
): number[][] => {
  return [...orders].sort(([aprice, _a], [bprice, _b]) => {
    if (type === OrderType.ASK) {
      return aprice - bprice
    } else {
      return bprice - aprice
    }
  })
}
