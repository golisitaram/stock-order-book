export const updateOrInsertOrder = (
  orders: number[][],
  updateOrder: number[],
) => {
  let currentOrders = [...orders]
  const [uPrice] = updateOrder

  const idx: number = currentOrders.map(([price]) => price).indexOf(uPrice)

  if (idx > -1) {
    const [price] = currentOrders[idx]
    if (price === uPrice) {
      currentOrders[idx] = updateOrder
    }
  } else {
    currentOrders = [...orders, updateOrder]
  }

  return currentOrders
}
