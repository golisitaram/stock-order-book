const getMaxNumber = (arr: number[]): number => {
  let max = arr[0]
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] > max) {
      max = arr[i]
    }
  }
  return max
}

const getMinNumber = (arr: number[]): number => {
  let min = arr[0]
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] < min) {
      min = arr[i]
    }
  }
  return min
}

export const getPeak = {
  max: (arr: number[]) => getMaxNumber(arr),
  min: (arr: number[]) => getMinNumber(arr),
}
