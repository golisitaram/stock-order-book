import { FeedRequest } from 'src/interfaces'

export const TableHeaderOptions: string[] = ['Total', 'Size', 'Price']

export enum OrderType {
  BID = 'BID',
  ASK = 'ASK',
}

export enum Product {
  ETHUSD = 'ETH-USD',
  BTCUSD = 'BTC-USD',
}

export const feedRequest: FeedRequest = {
  type: 'subscribe',
  product_ids: ['BTC-USD'],
  channels: ['level2'],
}

export const MAX_ORDERS = 25

export const tickOptions = [0.5, 1, 1.5, 2, 2.5]

export const sizeOptions = [25, 50, 75]
