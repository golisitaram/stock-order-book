import { useEffect, useState } from 'react'

export const useMediaQuery = (mediaQuery: string) => {
  const [isVerified, setIsVerified] = useState(
    Boolean(window.matchMedia(mediaQuery).matches),
  )

  useEffect(() => {
    const mediaQueryList = window.matchMedia(mediaQuery)
    const documentChangeHandler = () =>
      setIsVerified(Boolean(mediaQueryList.matches))

    mediaQueryList.addListener(documentChangeHandler)

    documentChangeHandler()
    return () => {
      mediaQueryList.removeListener(documentChangeHandler)
    }
  }, [mediaQuery])

  return isVerified
}
