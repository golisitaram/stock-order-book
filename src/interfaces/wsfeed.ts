export interface FeedRequest {
  type: string
  product_ids: string[]
  channels: string[]
}

export type FeedSnapshot = {
  type: string
  product_id: string
  bids: string[][]
  asks: string[][]
}

export type FeedUpdate = {
  type: string
  product_id: string
  time: string
  changes: string[][]
}

export type FeedSubscriptions = {
  type: string
  channels: Channel[]
}

export type Feed = FeedSnapshot & FeedUpdate & FeedSubscriptions

export type Channel = {
  name: string
  product_ids: string[]
}
