import { Product, tickOptions, sizeOptions } from 'src/constants'
export interface OrderBookState {
  market: Product
  tick: typeof tickOptions[number]
  snapAsks: string[][]
  snapBids: string[][]
  groupedAsks: number[][]
  groupedBids: number[][]
  computedAsks: number[][]
  computedBids: number[][]
  loading: boolean
  ws: WebSocket
  isFeedKilled: boolean
  isMobile: boolean
  isConnectionFailed: boolean
  ordersSize: typeof sizeOptions[number]
}
