import { useEffect } from 'react'
import {
  BookHeader,
  OrderBook,
  ActionItems,
  FeedConnection,
} from 'src/components'
import { DashboardWrapper, DashboardBookWrapper } from 'src/styles'

export const Dashboard = () => {
  return (
    <DashboardWrapper>
      <FeedConnection />
      <ActionItems />
      <DashboardBookWrapper>
        <BookHeader />
        <OrderBook />
      </DashboardBookWrapper>
    </DashboardWrapper>
  )
}
