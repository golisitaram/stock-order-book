import { Dashboard } from 'src/pages'
import { Loader } from 'src/components'

function App() {
  return (
    <>
      <Dashboard />
      <Loader />
    </>
  )
}

export default App
