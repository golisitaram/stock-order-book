import { OrderBookWrapper, ConnectionError } from 'src/styles'
import { OrderInfo } from '../OrderInfo'
import { OrderType } from 'src/constants'
import { useTypedSelector } from 'src/hooks'
import { SpreadInfo } from 'src/components'

export const OrderBook = () => {
  const { computedAsks, computedBids, isMobile, isConnectionFailed } =
    useTypedSelector(state => state.orderBook)

  return !isConnectionFailed ? (
    <OrderBookWrapper data-testid="order-book">
      <OrderInfo
        type={OrderType.BID}
        orders={computedBids}
        isMobile={isMobile}
      />
      {isMobile && <SpreadInfo />}
      <OrderInfo
        type={OrderType.ASK}
        orders={computedAsks}
        isMobile={isMobile}
      />
    </OrderBookWrapper>
  ) : (
    <ConnectionError>
      Oops..., Feed Connection has issues. please generate feed again.
    </ConnectionError>
  )
}
