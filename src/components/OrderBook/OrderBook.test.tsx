import { render, screen } from '@testing-library/react'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import { OrderBook } from '.'

const mock = configureStore()

test('Non Mobile View: Should render Order Book asks and bids tables without spread info', () => {
  const initialState = {
    orderBook: {
      isMobile: false,
      computedAsks: [[]],
      computedBids: [[]],
      isConnectionFailed: false,
    },
  }
  const store = mock(initialState)
  render(
    <Provider store={store}>
      <OrderBook />
    </Provider>,
  )
  const orderbook = screen.getByTestId('order-book')
  expect(orderbook.childElementCount).toEqual(2)
})

test('Mobile View: Should render Order Book asks and bids tables along with spread info', () => {
  const initialState = {
    orderBook: {
      isMobile: true,
      computedAsks: [[]],
      computedBids: [[]],
      isConnectionFailed: false,
    },
  }
  const store = mock(initialState)
  render(
    <Provider store={store}>
      <OrderBook />
    </Provider>,
  )
  const orderbook = screen.getByTestId('order-book')
  expect(orderbook.childElementCount).toEqual(3)
})
