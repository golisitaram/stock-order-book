import { useDispatch } from 'react-redux'
import { ActionsItemsWrapper, Button, Select as SelectSize } from 'src/styles'
import { ToggleFeed } from 'src/components'
import { killFeed, setOrderSize } from 'src/store/orderBook/slice'
import { useTypedSelector } from 'src/hooks'
import { sizeOptions } from 'src/constants'

export const ActionItems = () => {
  const dispatch = useDispatch()
  const { isFeedKilled, ordersSize } = useTypedSelector(
    state => state.orderBook,
  )

  return (
    <ActionsItemsWrapper>
      <ToggleFeed />

      {isFeedKilled ? (
        <Button
          type="button"
          background="green"
          onClick={() => dispatch(killFeed(false))}
        >
          Generate Feed
        </Button>
      ) : (
        <Button
          type="button"
          background="red"
          onClick={() => dispatch(killFeed(true))}
        >
          Kill Feed
        </Button>
      )}

      <SelectSize
        data-testid="select-orders"
        onChange={e => dispatch(setOrderSize(Number(e?.target?.value)))}
        defaultValue={ordersSize}
        name="orderSize"
      >
        {sizeOptions.map(size => (
          <option key={size} value={size}>
            {size} Orders
          </option>
        ))}
      </SelectSize>
    </ActionsItemsWrapper>
  )
}
