import { render, screen } from '@testing-library/react'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import { ActionItems } from '.'

const mock = configureStore()

test('Should render toggle feed selection and Kill feed button', () => {
  const initialState = {
    orderBook: {
      isFeedKilled: false,
    },
  }
  const store = mock(initialState)

  render(
    <Provider store={store}>
      <ActionItems />
    </Provider>,
  )
  const button = screen.getByText(/Kill Feed/i)
  expect(button).toBeInTheDocument()
})

test('Should render toggle feed selection and Generate feed button', () => {
  const initialState = {
    orderBook: {
      isFeedKilled: true,
    },
  }
  const store = mock(initialState)

  render(
    <Provider store={store}>
      <ActionItems />
    </Provider>,
  )
  const button = screen.getByText(/Generate Feed/i)
  expect(button).toBeInTheDocument()
})

test('Should render dropdown to select 50 orders', () => {
  const initialState = {
    orderBook: {
      ordersSize: 50,
    },
  }
  const store = mock(initialState)

  render(
    <Provider store={store}>
      <ActionItems />
    </Provider>,
  )
  const selectOrdersOptions = screen.getByRole('option', { name: '50 Orders' })
  expect((selectOrdersOptions as HTMLOptionElement).selected).toBe(true)
})

test('Should render dropdown to select 25 orders', () => {
  const initialState = {
    orderBook: {
      ordersSize: 25,
    },
  }
  const store = mock(initialState)

  render(
    <Provider store={store}>
      <ActionItems />
    </Provider>,
  )
  const selectOrdersOptions = screen.getByRole('option', { name: '50 Orders' })
  expect((selectOrdersOptions as HTMLOptionElement).selected).toBe(false)
})
