import { FC, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useTypedSelector, useMediaQuery } from 'src/hooks'
import useActions from 'src/store/orderBook/useActions'
import { setIsMobile } from 'src/store/orderBook/slice'
import { isEmptyObj, device } from 'src/utils'

export const FeedConnection: FC = () => {
  const dispatch = useDispatch()
  const isMd = useMediaQuery(device.md)

  const { market, isFeedKilled, ws } = useTypedSelector(
    state => state.orderBook,
  )

  const { initiateFeed, terminateFeed } = useActions()

  useEffect(() => {
    if (isFeedKilled) {
      terminateFeed()
    } else {
      const getCurrentMarket: string = localStorage.getItem('market') as string

      if (!isEmptyObj(ws)) {
        initiateFeed({
          type: 'unsubscribe',
          product_ids: [getCurrentMarket || market],
          channels: ['level2_batch'],
        })
      }
      initiateFeed({
        type: 'subscribe',
        product_ids: [market],
        channels: ['level2_batch'],
      })
    }
  }, [market, isFeedKilled])

  useEffect(() => {
    dispatch(setIsMobile(isMd))
  }, [isMd, dispatch])

  return null
}
