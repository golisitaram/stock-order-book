import { FC, useMemo } from 'react'
import { SpreadInfoWrapper } from 'src/styles'
import { useTypedSelector } from 'src/hooks'
import { getPeak } from 'src/utils'

export const SpreadInfo: FC = () => {
  const { computedAsks, computedBids, isConnectionFailed } = useTypedSelector(
    state => state.orderBook,
  )

  const getMaxBid = useMemo(() => {
    return getPeak.max(computedBids.map(([price]: number[]) => price))
  }, [computedBids])

  const getMinAsk = useMemo(() => {
    return getPeak.min(computedAsks.map(([price]: number[]) => price))
  }, [computedAsks])

  const spreadAmount = useMemo(() => {
    return Math.abs(getMaxBid - getMinAsk) || 0
  }, [getMaxBid, getMinAsk])

  const spreadPercentage = useMemo(() => {
    const calcPercent = ((spreadAmount * 100) / getMaxBid).toFixed(2)
    return isNaN(Number(calcPercent)) ? 0 : calcPercent
  }, [getMaxBid, spreadAmount])

  return !isConnectionFailed ? (
    <SpreadInfoWrapper data-testid="spread-info">
      <label>Spread: &nbsp;</label>
      <span>
        {spreadAmount} &nbsp;({spreadPercentage}%)
      </span>
    </SpreadInfoWrapper>
  ) : null
}
