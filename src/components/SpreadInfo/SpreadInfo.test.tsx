import { render, screen } from '@testing-library/react'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import { SpreadInfo } from '.'
import { getPeak } from 'src/utils'

const mock = configureStore()
const initialState = {
  orderBook: {
    computedAsks: [
      [23446.5, 0.02034261, 0.02034261, 0.06783452998710547],
      [23449, 0.36134785999999997, 0.38169046999999995, 1.2727862173539866],
    ],
    computedBids: [
      [23449, 0.8350000000000001, 0.8350000000000001, 2.321166708231999],
      [23448.5, 0.4, 1.235, 3.433102855888047],
    ],
    isConnectionFailed: false,
  },
}

const getMaxBid = getPeak.max(
  initialState.orderBook.computedBids.map(([price]) => price),
)
const getMinAsk = getPeak.min(
  initialState.orderBook.computedAsks.map(([price]) => price),
)
const spreadAmount = Math.abs(getMaxBid - getMinAsk)
const spreadPercentage = ((spreadAmount * 100) / getMaxBid).toFixed(2)

test('Should render Spread Info with values', () => {
  const store = mock(initialState)
  render(
    <Provider store={store}>
      <SpreadInfo />
    </Provider>,
  )
  const spreadInfo = screen.getByTestId('spread-info')
  const label = spreadInfo.getElementsByTagName('label')[0]
  const span = spreadInfo.getElementsByTagName('span')[0]
  expect(label).toHaveTextContent('Spread')
  expect(span).toHaveTextContent(`${spreadAmount} (${spreadPercentage}%)`)
})
