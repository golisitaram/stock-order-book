import styled from 'styled-components'
import { useDispatch } from 'react-redux'
import { setMarket } from 'src/store/orderBook/slice'
import { Product } from 'src/constants'
import { useTypedSelector } from 'src/hooks'
import { device } from 'src/utils'

const ToggleFeedElem = styled.div`
  width: 200px;
  height: 30px;

  input#market {
    display: none;

    // Unchecked State
    + label {
      height: 100%;
      width: 100%;
      > .toggle-market {
        cursor: pointer;
        width: 100%;
        height: 100%;
        position: relative;
        background-color: #e0e0e0;
        color: #555;
        transition: all 0.5s ease;
        padding: 3px;
        border-radius: 3px;
        box-shadow: inset 5px 5px 10px #b7b7b7, inset -5px -5px 10px #ccc;

        &:before,
        &:after {
          border-radius: 2px;
          height: calc(100% - 6px);
          width: calc(50% - 3px);
          display: flex;
          align-items: center;
          position: absolute;
          justify-content: center;
          transition: all 0.3s ease;
        }

        &:before {
          background-color: #e58e26;
          color: #fff;
          font-weight: bold;
          left: 3px;
          z-index: 10;
          content: attr(data-unchecked);
        }

        &:after {
          right: 0;
          content: attr(data-checked);
        }
      }
    }

    // Checked stae
    &:checked + label > .toggle-market {
      &:after {
        left: 0;
        content: attr(data-unchecked);
      }

      &:before {
        color: #fff;
        font-weight: bold;
        left: 50%;
        content: attr(data-checked);
      }
    }
  }

  @media ${device.sm} {
    width: 150px;
  }
`

export const ToggleFeed = () => {
  const dispatch = useDispatch()
  const { market } = useTypedSelector(state => state.orderBook)

  const handleToggleSelection = (e: React.ChangeEvent<HTMLInputElement>) => {
    const selectedMarket = e?.target?.checked ? Product.ETHUSD : Product.BTCUSD
    dispatch(setMarket(selectedMarket))
  }

  return (
    <ToggleFeedElem>
      <input
        id="market"
        type="checkbox"
        name="market"
        onChange={handleToggleSelection}
        defaultChecked={market !== Product.BTCUSD}
      />
      <label htmlFor="market">
        <div
          className="toggle-market"
          data-unchecked={Product.BTCUSD}
          data-checked={Product.ETHUSD}
        ></div>
      </label>
    </ToggleFeedElem>
  )
}
