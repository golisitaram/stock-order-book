import styled from 'styled-components'
import { useTypedSelector } from 'src/hooks'

const LoadWrapper = styled.div`
  width: 100%;
  height: 100vh;
  top: 0;
  left: 0;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #ffffff96;
`
const LoadElem = styled.div`
  border: 10px solid #aaa;
  border-top: 10px solid #555;
  border-radius: 50%;
  width: 80px;
  height: 80px;
  animation: spin 2s linear infinite;

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`

export const Loader = () => {
  const { loading } = useTypedSelector(state => state.orderBook)

  return loading ? (
    <LoadWrapper>
      <LoadElem />
    </LoadWrapper>
  ) : null
}
