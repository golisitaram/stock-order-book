import { render, screen } from '@testing-library/react'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import { BookHeader } from '.'

const mock = configureStore()

const initialState = {
  orderBook: {
    isMobile: false,
    computedAsks: [[]],
    computedBids: [[]],
    isConnectionFailed: false,
  },
}

test('Should render Order Book text in the header', () => {
  const store = mock(initialState)
  render(
    <Provider store={store}>
      <BookHeader />
    </Provider>,
  )
  const headText = screen.getByText(/Order Book/i)
  expect(headText).toBeInTheDocument()
})

test('Non Mobile View: Should render three elements', () => {
  const store = mock(initialState)

  render(
    <Provider store={store}>
      <BookHeader />
    </Provider>,
  )
  const header = screen.getByTestId('order-book-header')
  expect(header.childElementCount).toEqual(3)
})

test('Non Mobile View: Should render three elements', () => {
  const newInitStateWithMobile = {
    orderBook: {
      isMobile: true,
      computedAsks: [[]],
      computedBids: [[]],
      isConnectionFailed: false,
    },
  }
  const store = mock(newInitStateWithMobile)

  render(
    <Provider store={store}>
      <BookHeader />
    </Provider>,
  )
  const header = screen.getByTestId('order-book-header')
  expect(header.childElementCount).toEqual(2)
})
