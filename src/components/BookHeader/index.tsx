import { useDispatch } from 'react-redux'
import { BookHeaderWrapper, Select as SelectTicker } from 'src/styles'
import { SpreadInfo } from 'src/components'
import { tickOptions } from 'src/constants'
import { setTick } from 'src/store/orderBook/slice'
import { useTypedSelector } from 'src/hooks'

export const BookHeader = () => {
  const { isMobile, tick } = useTypedSelector(state => state.orderBook)
  const dispatch = useDispatch()

  return (
    <BookHeaderWrapper data-testid="order-book-header">
      <h3>Order Book</h3>
      {!isMobile && <SpreadInfo />}
      <SelectTicker
        onChange={e => dispatch(setTick(Number(e?.target?.value)))}
        defaultValue={tick}
      >
        {tickOptions.map(tick => (
          <option key={tick} value={tick}>
            Group {tick}
          </option>
        ))}
      </SelectTicker>
    </BookHeaderWrapper>
  )
}
