import { FC, useMemo } from 'react'
import { OrderInfoTable, OrderTableRow } from 'src/styles'
import { TableHeaderOptions, OrderType } from 'src/constants'
import { formatPrice } from 'src/utils'

interface Props {
  type: OrderType
  orders: number[][]
  isMobile: boolean
}
export const OrderInfo: FC<Props> = ({
  type = OrderType.BID,
  orders,
  isMobile,
}) => {
  const headers = useMemo(() => {
    return type === OrderType.BID
      ? TableHeaderOptions
      : TableHeaderOptions.slice().reverse()
  }, [type])

  return (
    <OrderInfoTable
      border={0}
      cellSpacing="0"
      cellPadding="0"
      data-testid={`${type}_table`}
    >
      {type === OrderType.BID && isMobile ? null : (
        <thead>
          <tr>
            {headers?.map((th: string) => (
              <th key={th}>{th}</th>
            ))}
          </tr>
        </thead>
      )}
      <tbody>
        {orders.map(([price, size, total, depth]) => (
          <OrderTableRow
            key={price + size + total}
            type={type}
            depth={depth}
            isMobile={isMobile}
          >
            <td>
              {type === OrderType.BID && !isMobile ? total : formatPrice(price)}
            </td>
            <td>{size}</td>
            <td>
              {type === OrderType.BID && !isMobile ? formatPrice(price) : total}
            </td>
          </OrderTableRow>
        ))}
      </tbody>
    </OrderInfoTable>
  )
}
