import { render, screen } from '@testing-library/react'
import { OrderInfo } from '.'
import { OrderType } from 'src/constants'

const bidOrderType = OrderType.BID
const bidOrder = [[23563, 0.35, 1.25, 10]]
test('Non Mobile View: Should Render Bids table', () => {
  render(<OrderInfo type={bidOrderType} orders={bidOrder} isMobile={false} />)
  const tableElem = screen.getByTestId(`${bidOrderType}_table`)
  expect(tableElem).toBeInTheDocument()
})

test('Mobile View: Should Render Bids table without header', () => {
  render(<OrderInfo type={bidOrderType} orders={bidOrder} isMobile={true} />)
  const tableElem = screen.getByTestId(`${bidOrderType}_table`)
  const tableHeader = tableElem.getElementsByTagName('thead')
  expect(tableHeader).toHaveLength(0)
})

const askOrderType = OrderType.ASK
const askOrder = [[23563, 0.35, 1.25, 10]]
test('Non Mobile View: Should Render Asks table', () => {
  render(<OrderInfo type={askOrderType} orders={askOrder} isMobile={false} />)
  const tableElem = screen.getByTestId(`${askOrderType}_table`)
  expect(tableElem).toBeInTheDocument()
})

test('Mobile View: Should Render Asks table with header', () => {
  render(<OrderInfo type={askOrderType} orders={askOrder} isMobile={true} />)
  const tableElem = screen.getByTestId(`${askOrderType}_table`)
  const tableHeader = tableElem.getElementsByTagName('thead')
  expect(tableHeader).toHaveLength(1)
})
