import {
  call,
  put,
  select,
  takeLatest,
  take,
  cancelled,
  all,
} from 'redux-saga/effects'
import * as actions from './slice'
import { Feed } from 'src/interfaces'
import { OrderType, Product } from 'src/constants'
import { RootState } from 'src/store'
import { createWebSocket, socketInstance } from 'src/services'
import {
  isEmptyObj,
  updateOrInsertOrder,
  filterZeroSizeOrders,
  groupByTickPrice,
  setTotalSize,
  setDepth,
  compareOrdersByPrice,
  getMaxTotalSize,
} from 'src/utils'

const BASE_ORDERS_SIZE = 500

export const getOrderBookState = (state: RootState) => state.orderBook

function updateOrders(
  orders: number[][],
  orderUpdates: number[][],
): number[][] {
  let newOrders = orders

  for (let update of orderUpdates) {
    const [uPrice, uSize] = update
    if (uSize === 0) {
      newOrders = filterZeroSizeOrders(uPrice, orders)
    } else {
      newOrders = updateOrInsertOrder(orders, update)
    }
  }

  return newOrders
}

function computeOrders(orders: number[][]): number[][] {
  const ordersByTotalSize: number[][] = setTotalSize(orders)
  const maxTotal: number = getMaxTotalSize(ordersByTotalSize)
  return setDepth(ordersByTotalSize, maxTotal)
}

function processOrdes(
  grouped: number[][],
  updates: string[][],
  tick: number,
  type: OrderType,
): number[][] {
  const updatesByTick = groupByTickPrice(updates, tick)
  const updated = updateOrders(grouped, updatesByTick)
  const sorted = compareOrdersByPrice(updated, type)
  return computeOrders(sorted)
}

export function* initiateFeed(
  action: ReturnType<typeof actions.initiateFeed>,
): any {
  const { payload } = action
  const { ws } = yield select(getOrderBookState)

  yield put(actions.setLoading(true))

  let socket
  let socketChannel

  try {
    socket = !isEmptyObj(ws) ? ws : yield call(createWebSocket)
    socketChannel = yield call(socketInstance, socket, payload)

    yield put(actions.setWS(socket))
    yield put(actions.setIsConnectionFailed(false))

    while (true) {
      const { tick, isMobile, loading, groupedAsks, groupedBids } =
        yield select(getOrderBookState)
      const payload: Feed = yield take(socketChannel)

      if (payload && loading) {
        yield put(actions.setLoading(false))
      }

      if (payload?.type === 'snapshot') {
        const [limitedAsks, limitedBids] = [
          payload?.asks.slice(0, BASE_ORDERS_SIZE),
          payload?.bids.slice(0, BASE_ORDERS_SIZE),
        ]
        yield all([
          put(actions.setSnapAsks(limitedAsks)),
          put(actions.setSnapBids(limitedBids)),
          put(actions.setMarket(payload?.product_id as Product)),
        ])
        yield localStorage.setItem('market', payload?.product_id)
      }

      if (payload?.type === 'l2update') {
        const [askUpdates, bidUpdates] = yield payload?.changes.reduce(
          (a: string[][][], c: string[]) => {
            const [side, price, size] = c
            if (side === 'buy') {
              a[0].push([price, size])
            } else {
              a[1].push([price, size])
            }
            return a
          },
          [[], []],
        )

        const [computedAsks, computedBids] = yield all([
          call(processOrdes, groupedAsks, askUpdates, tick, OrderType.ASK),
          call(processOrdes, groupedBids, bidUpdates, tick, OrderType.BID),
        ])

        yield all([
          put(
            actions.updateAsks(
              isMobile ? computedAsks.reverse() : computedAsks,
            ),
          ),
          put(actions.updateBids(computedBids)),
        ])
      }
    }
  } catch (e) {
    console.error(e)
    yield put(actions.setLoading(false))
    yield put(actions.setIsConnectionFailed(true))
    yield put(actions.killFeed(true))
  } finally {
    if (yield cancelled()) {
      socketChannel.close()
    }
  }
}

export function* terminateFeed(action: ReturnType<typeof actions.killFeed>) {
  try {
    const { ws } = yield select(getOrderBookState)

    if (ws instanceof WebSocket) {
      yield ws.close()
      yield put(actions.setWS({} as WebSocket))
    }
  } catch (e) {
    console.error(e)
  }
}

export function* watchOrderBook() {
  yield takeLatest(actions.initiateFeed.type, initiateFeed)
  yield takeLatest(actions.terminateFeed.type, terminateFeed)
}
