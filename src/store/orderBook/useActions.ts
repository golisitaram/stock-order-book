import { useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'

import { initiateFeed, terminateFeed, clearStore } from './slice'

export default () => {
  const dispatch = useDispatch()

  return bindActionCreators(
    { initiateFeed, terminateFeed, clearStore },
    dispatch,
  )
}
