import {
  PayloadAction,
  createAction,
  createSlice,
  current,
} from '@reduxjs/toolkit'
import { OrderBookState, FeedRequest } from 'src/interfaces'
import { MAX_ORDERS, Product } from 'src/constants'
import { groupByTickPrice } from 'src/utils'

const prefix = 'orderBook'

const initialState: OrderBookState = {
  market: Product.BTCUSD,
  tick: 0.5,
  snapAsks: [],
  snapBids: [],
  groupedAsks: [],
  groupedBids: [],
  computedAsks: [],
  computedBids: [],
  loading: false,
  ws: {} as WebSocket,
  isFeedKilled: false,
  isMobile: false,
  isConnectionFailed: false,
  ordersSize: MAX_ORDERS,
}

export const clearStore = createAction(`${prefix}/clearStore`)
export const initiateFeed = createAction<FeedRequest>(`${prefix}/initiateFeed`)
export const terminateFeed = createAction(`${prefix}/terminateFeed`)

export const orderBookSlice = createSlice({
  name: 'orderBook',
  initialState,
  reducers: {
    setWS: (state, { payload }: PayloadAction<WebSocket>) => {
      state.ws = payload
    },
    setMarket: (state, { payload }: PayloadAction<Product>) => {
      state.market = payload
    },
    setTick: (state, { payload }: PayloadAction<number>) => {
      const { snapAsks, snapBids, ordersSize } = current(state)
      const groupedAsks = groupByTickPrice(snapAsks, payload, ordersSize)
      const groupedBids = groupByTickPrice(snapBids, payload, ordersSize)
      state.tick = payload
      state.groupedAsks = groupedAsks
      state.groupedBids = groupedBids
    },
    setSnapAsks: (state, { payload }: PayloadAction<string[][]>) => {
      const { tick, ordersSize } = current(state)
      const groupedAsks = groupByTickPrice(payload, tick, ordersSize)
      state.snapAsks = payload
      state.groupedAsks = groupedAsks
    },
    updateAsks: (state, { payload }: PayloadAction<number[][]>) => {
      state.computedAsks = payload
    },
    setSnapBids: (state, { payload }: PayloadAction<string[][]>) => {
      const { tick, ordersSize } = current(state)
      const groupedBids = groupByTickPrice(payload, tick, ordersSize)
      state.snapBids = payload
      state.groupedBids = groupedBids
    },
    updateBids: (state, { payload }: PayloadAction<number[][]>) => {
      state.computedBids = payload
    },
    killFeed: (state, { payload }: PayloadAction<boolean>) => {
      state.isFeedKilled = payload
    },
    setLoading: (state, { payload }: PayloadAction<boolean>) => {
      state.loading = payload
    },
    setIsMobile: (state, { payload }: PayloadAction<boolean>) => {
      state.isMobile = payload
    },
    setIsConnectionFailed: (state, { payload }: PayloadAction<boolean>) => {
      state.isConnectionFailed = payload
    },
    setOrderSize: (state, { payload }: PayloadAction<number>) => {
      const { snapAsks, snapBids, tick } = current(state)
      const groupedAsks = groupByTickPrice(snapAsks, tick, payload)
      const groupedBids = groupByTickPrice(snapBids, tick, payload)
      state.ordersSize = payload
      state.groupedAsks = groupedAsks
      state.groupedBids = groupedBids
    },
  },
  extraReducers: {
    [clearStore.type]: () => initialState,
  },
})

export const {
  setMarket,
  setTick,
  setSnapAsks,
  updateAsks,
  setSnapBids,
  updateBids,
  setWS,
  killFeed,
  setLoading,
  setIsMobile,
  setIsConnectionFailed,
  setOrderSize,
} = orderBookSlice.actions

export default orderBookSlice.reducer
