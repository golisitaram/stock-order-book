import { configureStore } from '@reduxjs/toolkit'
import saga from 'redux-saga'

import orderBookReducer from './orderBook/slice'
import RootSaga from './RootSaga'

const sagaMiddleware = saga()

const store = configureStore({
  reducer: {
    orderBook: orderBookReducer,
  },
  middleware: [sagaMiddleware],
  devTools: true,
})

sagaMiddleware.run(RootSaga)

export type RootState = ReturnType<typeof store.getState>

export default store
