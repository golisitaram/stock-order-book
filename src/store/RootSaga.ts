import { all, fork } from 'redux-saga/effects'
import { watchOrderBook } from 'src/store/orderBook/sagas'

export default function* RootSaga() {
  yield all([fork(watchOrderBook)])
}
