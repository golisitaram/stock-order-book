import styled from 'styled-components'
import { OrderType } from 'src/constants'
import { device } from 'src/utils'

export const DashboardWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  flex-flow: column;
  row-gap: 2em;
  margin-top: 20px;
  width: 70%;

  @media ${device.lg} {
    width: 80%;
  }

  @media ${device.md} {
    width: 90%;
  }

  @media ${device.sm} {
    width: 95%;
  }
`

export const DashboardBookWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;

  border-radius: 2em;
  background: transparent;
  box-shadow: 20px 20px 60px #bebebe, -20px -20px 60px #ffffff;
  margin-bottom: 20px;

  & > div:not(:last-child) {
    border-bottom: 1px solid #ccc;
  }
`

export const BookHeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  padding: 1em;
`

export const SpreadInfoWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  @media ${device.md} {
    width: 100%;
    background: #999999;
    color: #fff;
    padding: 5px 0;
  }
`

export const Select = styled.select`
  background: #e0e0e0;
  box-shadow: inset 5px 5px 10px #bebebe, inset -5px -5px 10px #ffffff;
  border: none;
  border-radius: 5px;
  padding: 5px;
  width: 120px;
`

export const OrderBookWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-start;
  width: 100%;
  column-gap: 2px;

  & > table {
    &: first-child {
      order: 1;
    }
    &: nth-child(2) {
      order: 2;
    }
  }

  @media ${device.md} {
    flex-direction: column;
    column-gap: 0;
    row-gap: 5px;
    align-items: center;

    & > table {
      &: first-child {
        order: 2;
      }
    }

    & > div {
      order: 1;
    }
  }
`

export const OrderInfoTable = styled.table`
  width: 50%;
  text-align: center;

  & th,
  td {
    padding: 5px;
    width: 33.33%;
  }

  @media ${device.md} {
    width: 100%;
  }
`

interface TableRowProps {
  type: string
  depth: number
  isMobile: boolean
}

export const OrderTableRow = styled.tr.attrs<TableRowProps>(
  ({ type, depth, isMobile }) => ({
    style: {
      background:
        type === OrderType.BID
          ? `linear-gradient(
    to ${isMobile ? 'right' : 'left'},
    #079992 0%,
    #079992 ${depth}%,
    transparent ${depth}%
  )`
          : `linear-gradient(
    to right,
    #e55039 0%,
    #e55039 ${depth}%,
    transparent ${depth}%
  )`,
    },
  }),
)<TableRowProps>``

export const ActionsItemsWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  column-gap: 20px;

  @media ${device.md} {
    justify-content: space-between;
  }
`

interface ButtonProps {
  background: string
}
export const Button = styled.button.attrs((props: ButtonProps) => ({
  style: {
    background: props.background || '#0a3d62',
  },
}))<ButtonProps>`
  border: none;
  padding: 8px 10px;
  border-radius: 5px;
  background: ;
  color: #e0e0e0;
  min-width: 100px;
  cursor: pointer;

  @media ${device.xs} {
    padding: 8px 5px;
    min-width: 80px;
  }
`

export const ConnectionError = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: red;
  font-size: 2em;
  padding: 2em;
`
