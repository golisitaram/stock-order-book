import { EventChannel, eventChannel, END } from 'redux-saga'

const feedURL: string = 'wss://ws-feed.exchange.coinbase.com'

export function createWebSocket() {
  return new Promise((resolve, reject) => {
    const socket = new WebSocket(feedURL)

    socket.onopen = function () {
      resolve(socket)
    }

    socket.onerror = function (err) {
      reject(err)
    }
  })
}

export function socketInstance(socket: WebSocket, reqData: any) {
  return eventChannel(emit => {
    socket.send(JSON.stringify(reqData))

    socket.onmessage = event => {
      if (reqData?.type === 'unsubscribe') {
        emit(END)
      }
      emit(JSON.parse(event.data))
    }

    socket.onclose = () => {
      emit(END)
    }

    const unsubscribe = () => {
      socket.onmessage = null
      emit(END)
    }

    return unsubscribe
  })
}
